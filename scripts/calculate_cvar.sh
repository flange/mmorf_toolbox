#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate voxelwise CVAR values
source ~/.bash_profile
conda activate neuro

sub_id=$1
reg_id=$2

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
sub_dir="${project_dir}/data/${sub_id}"
output_dir="${sub_dir}"

# Create output directory
echo ">>> Creating directory ${output_dir}"
if ! [[ -d ${output_dir} ]];
then
  mkdir -p ${output_dir}
else
  echo "Directory already exists"
fi

# Common files
ref_file="${project_dir}/template/t1"
out_file="${output_dir}/cvar_${reg_id}"

# Method specifics
echo ">>> Running calculate_cvar.py"
if [[ ${reg_id} == "ants" ]]
then
    warp_file="${sub_dir}/ants/ants_t1_1Warp"
    ${project_dir}/scripts/calculate_cvar.py \
        -w ${warp_file} \
        -r ${ref_file} \
        -o ${out_file} \
        -i
elif [[ ${reg_id} == "drtamas" ]]
then
    warp_file="${sub_dir}/drtamas/warp_drtamas_orig"
    ${project_dir}/scripts/calculate_cvar.py \
        -w ${warp_file} \
        -r ${ref_file} \
        -o ${out_file} \
        -i
else
    warp_file="${sub_dir}/warp_${reg_id}"
    ${project_dir}/scripts/calculate_cvar.py \
        -w ${warp_file} \
        -r ${ref_file} \
        -o ${out_file}
fi

echo ">>> Reg ${reg_id}, Sub ${sub_id} completed"