
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Affine register each subject to the template
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
hcp_dir="/well/win-hcp/HCP-YA/subjectsAll"
subject_t1_dir="${hcp_dir}/${subject_id}/T1w"
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
out_dir="${project_dir}/data/${subject_id}"

# Create output directory
echo ">>> Creating directory ../data/${subject_id}"
if ! [[ -d ../data/${subject_id} ]];
then
  mkdir -p ../data/${subject_id}
else
  echo "Directory already exists"
fi

# FLIRT subject to template
refname="${project_dir}/template/t1"
infname="${subject_t1_dir}/T1w_acpc_dc_restore_brain"
affname="${out_dir}/t1_to_template_affine.mat"
outname="${out_dir}/t1_to_template_affine"

echo ">>> Running FLIRT between subject and template T1w images"
flirt \
  -ref ${refname} \
  -in ${infname} \
  -omat ${affname} \
  -out ${outname} \
  -v
echo ">>> Subject ${subject_id} completed"