
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile

# Run randomise voxelwise for each cope
task_id=$1
cope_id=$2
smooth_id=$3
reg_id=$4
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
in_file="${project_dir}/data/group/tfmri/${task_id}_hp200_s${smooth_id}/cope${cope_id}_${reg_id}_merged"
out_dir="${project_dir}/data/group/tfmri/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_id}"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/mean_activation"
mask_file="${project_dir}/template/mask_t1"

echo ">>> Running randomise" 
randomise \
  -1 \
  -i ${in_file} \
  -o ${out_file} \
  -m ${mask_file} \
  -n 10000 \
  -x

echo ">>> Completed"