
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Decompose DTI volumes into eigensystem
source ~/.bash_profile

subject_id=$1
reg_id=$2

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
subject_dir="${project_dir}/data/${subject_id}"
out_dir="${subject_dir}/dti"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

in_file="${out_dir}/dti_tensor_${reg_id}"
out_prefix="${out_dir}/dti_tensor_${reg_id}_decomp"

# Run fslmaths
echo ">>> Running fslmaths"
fslmaths ${in_file} -tensor_decomp ${out_prefix}

echo ">>> Subject ${subject_id} completed"