#!/bin/bash
# Calculate DTI similarity metrics between each subject and the template

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(af fnirt mmorf ants drtamas mmorf_fa_mask)

subject_id_file="../subjects.txt"

for reg_id in ${reg_id_list[@]} 
do
    submit_file="../submit/job_calculate_dti_metrics_${reg_id}.sh"
    if [[ -f ${submit_file} ]];
    then
        rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for subject_id in $(cat ${subject_id_file})
    do
        # Populate submit script
        echo "./calculate_dti_metrics.sh ${subject_id} ${reg_id}" >> ${submit_file}
    done
    # Submit task array job
    fsl_sub -q short.qc -t ${submit_file} -l logs/log_calculate_dti_metrics/${reg_id}
done