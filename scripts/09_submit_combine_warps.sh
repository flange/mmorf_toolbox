#!/bin/bash
# Parse list of HCP subjects and create array job for combining affine and nonliear warps

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(mmorf mmorf_fa_mask)

subject_id_file="../subjects.txt"

for reg_id in ${reg_id_list[@]} 
do
    submit_file="../submit/job_combine_warps_${reg_id}.sh"
    if [[ -f ${submit_file} ]];
    then
        rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for subject_id in $(cat ${subject_id_file})
    do
        # Populate submit script
        echo "./combine_warps.sh ${subject_id} ${reg_id}" >> ${submit_file}
    done
    # Submit task array job
    fsl_sub -q short.qc -t ${submit_file} -l logs/log_combine_warps/${reg_id}
done