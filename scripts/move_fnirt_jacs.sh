#!/bin/bash

# Loop over subjects
subject_id_file="../subjects.txt"
for subject_id in $(cat ${subject_id_file})
do
    immv ../data/${subject_id}/fnirt/jac_fnirt ../data/${subject_id}/.
done