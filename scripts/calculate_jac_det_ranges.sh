#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate 5th and 95th percentile of the Jacobian determinant within the brain mask
source ~/.bash_profile

reg_id=$1
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
mask="${project_dir}/template/mask_t1"
out_dir="${project_dir}/data/group/jacs"
out_file="${out_dir}/${reg_id}_jac_ranges.txt"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

# Calculate Jacobian determinant ranges
jacs=`ls ${project_dir}/data/*/jac_${reg_id}.nii.gz`

echo "5th 95th 2nd 98th 1st 99th" > ${out_file}
for jac in ${jacs}
do
  fslstats ${jac} -k ${mask} -p 5 -p 95 -p 2 -p 98 -p 1 -p 99 >> ${out_file}
done