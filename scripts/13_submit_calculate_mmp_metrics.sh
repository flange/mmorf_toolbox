#!/bin/bash
# Parse list of HCP subjects and create array job for calculating MMP1.0 metrics

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(af fnirt mmorf ants drtamas mmorf_fa_mask)

for reg_id in ${reg_id_list[@]} 
do
  submit_file="../submit/job_calculate_mmp_metrics_${reg_id}.sh"
  if [[ -f ${submit_file} ]];
  then
    rm ${submit_file}
  fi
  touch ${submit_file}
  chmod u+x $submit_file
  # Populate submit script
  echo "./calculate_mmp_metrics_mmp.sh ${reg_id}" >> ${submit_file}
  #echo "./calculate_mmp_metrics_fs.sh ${reg_id}" >> ${submit_file}
  # Submit task array job
  fsl_sub -q short.qc -t ${submit_file} -R 30 -l logs/log_calculate_mmp_metrics/${reg_id}
done