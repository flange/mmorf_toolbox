
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Transform DTI volumes into OMM space
source ~/.bash_profile

subject_id=$1
reg_id=$2

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
dti_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation"
subject_dti_dir="${dti_dir}/data/${subject_id}"
subject_dir="${project_dir}/data/${subject_id}"
out_dir="${subject_dir}/dti"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

ref_file="${project_dir}/template/t1"
mask_file="${project_dir}/template/mask_t1"
warp_file="${subject_dir}/warp_${reg_id}"
in_file="${subject_dti_dir}/dti_tensor"
out_file="${out_dir}/dti_tensor_${reg_id}"

# Run applywarp
echo ">>> Running vecreg"
if [[ ${reg_id} == "af" ]]
then
    vecreg \
    --input=${in_file} \
    --ref=${ref_file} \
    --output=${out_file} \
    --affine=${subject_dir}/t1_to_template_affine.mat \
    --interp=trilinear \
    --refmask=${mask_file} \
    --verbose
elif [[ ${reg_id} == "fnirt" ]]
then
    vecreg \
    --input=${in_file} \
    --ref=${ref_file} \
    --output=${out_file} \
    --warpfield=${warp_file} \
    --interp=trilinear \
    --refmask=${mask_file} \
    --verbose
else
    vecreg \
    --input=${in_file} \
    --ref=${ref_file} \
    --output=${out_file} \
    --warpfield=${warp_file}_combined \
    --interp=trilinear \
    --refmask=${mask_file} \
    --verbose
fi

echo ">>> Subject ${subject_id} completed"