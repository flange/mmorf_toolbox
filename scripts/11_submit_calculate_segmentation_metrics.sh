#!/bin/bash
# Calculate Hausdorff and Jaccard metrics of segmentation overlaps for all combinations
# of subjects for each registration method.

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(af fnirt mmorf ants drtamas mmorf_fa_mask)


subject_combo_file="../subject_combinations.txt"

for reg_id in ${reg_id_list[@]} 
do
    submit_file="../submit/job_calculate_segmentation_metrics_${reg_id}.sh"
    if [[ -f ${submit_file} ]];
    then
        rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    while read line
    do
        # Populate submit script
        echo "./calculate_segmentation_metrics.sh ${line} ${reg_id}" >> ${submit_file}
    done < ${subject_combo_file}
    # Submit task array job
    fsl_sub -q short.qc -t ${submit_file} -l logs/log_calculate_segmentation_metrics/${reg_id}
done