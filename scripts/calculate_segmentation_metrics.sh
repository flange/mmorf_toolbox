
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate Jaccard index and Hausdorff distance
source ~/.bash_profile
conda activate neuro

subject_a_id=$1
subject_b_id=$2
reg_id=$3

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
output_dir="${project_dir}/data/group/seg/${reg_id}"
subject_a_dir="${project_dir}/data/${subject_a_id}"
subject_b_dir="${project_dir}/data/${subject_b_id}"

# Create output directory
echo ">>> Creating directory ${output_dir}"
if ! [[ -d ${output_dir} ]];
then
  mkdir -p ${output_dir}
else
  echo "Directory already exists"
fi

# FreeSurfer Segmentations
output_csv="${output_dir}/${subject_a_id}_vs_${subject_b_id}_fs_seg.csv"
subject_a_seg="${subject_a_dir}/seg/fs/aparc.a2009s+aseg_${reg_id}"
subject_b_seg="${subject_b_dir}/seg/fs/aparc.a2009s+aseg_${reg_id}"
lut="${project_dir}/freesurferlut.csv"

# Run metric calculation
echo ">>> Running calculate_segmentation_metrics.py for FreeSurfer"
${project_dir}/scripts/calculate_segmentation_metrics.py \
    -a ${subject_a_seg} \
    -b ${subject_b_seg} \
    -A ${subject_a_id} \
    -B ${subject_b_id} \
    -l ${lut} \
    -r ${reg_id} \
    -o ${output_csv}

# HCP MMP 1.0 Left Segmentations
output_csv="${output_dir}/${subject_a_id}_vs_${subject_b_id}_mmp_L_seg.csv"
subject_a_seg="${subject_a_dir}/seg/mmp/hcp_mmp_seg_L_${reg_id}"
subject_b_seg="${subject_b_dir}/seg/mmp/hcp_mmp_seg_L_${reg_id}"
lut="${project_dir}/hcpmmp1lut.csv"

# Run metric calculation
echo ">>> Running calculate_segmentation_metrics.py for Left HCP MMP 1.0 segmentations"
${project_dir}/scripts/calculate_segmentation_metrics.py \
    -a ${subject_a_seg} \
    -b ${subject_b_seg} \
    -A ${subject_a_id} \
    -B ${subject_b_id} \
    -l ${lut} \
    -r ${reg_id} \
    -o ${output_csv}

# HCP MMP 1.0 Right Segmentations
output_csv="${output_dir}/${subject_a_id}_vs_${subject_b_id}_mmp_R_seg.csv"
subject_a_seg="${subject_a_dir}/seg/mmp/hcp_mmp_seg_R_${reg_id}"
subject_b_seg="${subject_b_dir}/seg/mmp/hcp_mmp_seg_R_${reg_id}"
lut="${project_dir}/hcpmmp1lut.csv"

# Run metric calculation
echo ">>> Running calculate_segmentation_metrics.py for Right HCP MMP 1.0 segmentations"
${project_dir}/scripts/calculate_segmentation_metrics.py \
    -a ${subject_a_seg} \
    -b ${subject_b_seg} \
    -A ${subject_a_id} \
    -B ${subject_b_id} \
    -l ${lut} \
    -r ${reg_id} \
    -o ${output_csv}

echo ">>> Subject ${subject_id} completed"