#!/bin/bash
# Calculate the 5th and 95th percentiles of the Jacobian determinant for each registration
# method

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(fnirt mmorf ants drtamas mmorf_fa_mask)

for reg_id in ${reg_id_list[@]} 
do
  submit_file="../submit/job_calculate_jac_det_ranges_${reg_id}.sh"
  if [[ -f ${submit_file} ]];
  then
    rm ${submit_file}
  fi
  touch ${submit_file}
  chmod u+x $submit_file
  # Populate submit script
  echo "./calculate_jac_det_ranges.sh ${reg_id}" >> ${submit_file}
  # Submit task array job
  fsl_sub -q short.qc -t ${submit_file} -l logs/log_calculate_jac_det_ranges/${reg_id}
done