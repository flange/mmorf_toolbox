
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Transform FreeSurfer and HCP MMP 1.0 segmentations into OMM space
source ~/.bash_profile

subject_id=$1
reg_id=$2

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
subject_dir="${project_dir}/data/${subject_id}"
hcp_dir="/well/win-hcp/HCP-YA/subjectsAll/${subject_id}/T1w"
mmp_dir="/well/win-fmrib-analysis/users/sgk882/multimodal_validation/data/${subject_id}/seg"
out_dir_fs="${subject_dir}/seg/fs"
out_dir_mmp="${subject_dir}/seg/mmp"

# Create output directories
echo ">>> Creating directories ${out_dir_fs} and ${out_dir_mmp}"
if ! [[ -d ${out_dir_fs} ]];
then
  mkdir -p ${out_dir_fs}
else
  echo "Directory already exists"
fi
if ! [[ -d ${out_dir_mmp} ]];
then
  mkdir -p ${out_dir_mmp}
else
  echo "Directory already exists"
fi

ref_file="${project_dir}/template/t1"
warp_file="${subject_dir}/warp_${reg_id}"

# FreeSurfer segmentations
in_file="${hcp_dir}/aparc.a2009s+aseg"
out_file="${out_dir_fs}/aparc.a2009s+aseg_${reg_id}"

# Run applywarp
echo ">>> Running applywarp for FreeSurfer segmentations"
if [[ ${reg_id} == "af" ]]
then
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --premat=${subject_dir}/t1_to_template_affine.mat \
    --interp=nn \
    --datatype=int \
    --verbose
elif [[ ${reg_id} == "fnirt" ]]
then
  applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file} \
    --interp=nn \
    --datatype=int \
    --verbose
else
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file}_combined \
    --interp=nn \
    --datatype=int \
    --verbose
fi

# HCP MMP segmentations
in_file="${mmp_dir}/${subject_id}.L.CorticalAreas_dil_Final_Individual"
out_file="${out_dir_mmp}/hcp_mmp_seg_L_${reg_id}"

# Run applywarp
echo ">>> Running applywarp for Left HCP MMP 1.0 segmentations"
if [[ ${reg_id} == "af" ]]
then
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --premat=${subject_dir}/t1_to_template_affine.mat \
    --interp=nn \
    --datatype=int \
    --verbose
elif [[ ${reg_id} == "fnirt" ]]
then
  applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file} \
    --interp=nn \
    --datatype=int \
    --verbose
else
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file}_combined \
    --interp=nn \
    --datatype=int \
    --verbose
fi

in_file="${mmp_dir}/${subject_id}.R.CorticalAreas_dil_Final_Individual"
out_file="${out_dir_mmp}/hcp_mmp_seg_R_${reg_id}"

# Run applywarp
echo ">>> Running applywarp for Right HCP MMP 1.0 segmentations"
if [[ ${reg_id} == "af" ]]
then
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --premat=${subject_dir}/t1_to_template_affine.mat \
    --interp=nn \
    --datatype=int \
    --verbose
elif [[ ${reg_id} == "fnirt" ]]
then
  applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file} \
    --interp=nn \
    --datatype=int \
    --verbose
else
    applywarp \
    --in=${in_file} \
    --ref=${ref_file} \
    --out=${out_file} \
    --warp=${warp_file}_combined \
    --interp=nn \
    --datatype=int \
    --verbose
fi

echo ">>> Subject ${subject_id} completed"