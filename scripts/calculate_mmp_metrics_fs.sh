#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate metrics for HCP FS segmentations
source ~/.bash_profile
conda activate neuro

reg_id=$1

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
output_dir="${project_dir}/data/group/seg/${reg_id}"

# Create output directory
echo ">>> Creating directory ${output_dir}"
if ! [[ -d ${output_dir} ]];
then
  mkdir -p ${output_dir}
else
  echo "Directory already exists"
fi

# Parameters for python script
seg_fs="${output_dir}/aparc.a2009s+aseg_${reg_id}_merged"
temp="${project_dir}/template/t1"
lut="${project_dir}/freesurferlut.csv"

# Run metric calculation
echo ">>> Running calculate_mmp_metrics_fs.py"
${project_dir}/scripts/calculate_mmp_metrics_fs.py \
    -s   ${seg_fs} \
    -t   ${temp} \
    -l   ${lut} \
    -r   ${reg_id} \
    -o   ${output_dir}

echo ">>> Reg ${reg_id} completed"