#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Calculate summary stats of voxelwise CVAR values
source ~/.bash_profile

reg_id=$1
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
mask="${project_dir}/template/mask_t1"
out_dir="${project_dir}/data/group/cvars"
out_file="${out_dir}/${reg_id}_cvar_stats.txt"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

# Calculate CVAR stats
cvars=`ls ${project_dir}/data/*/cvar_${reg_id}.nii.gz`

echo "Mean 50th 95th 99th" > ${out_file}
for cvar in ${cvars}
do
  fslstats ${cvar} -k ${mask} -m -p 50 -p 95 -p 99 >> ${out_file}
done