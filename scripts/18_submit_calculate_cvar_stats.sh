#!/bin/bash
# Calculate summary CVAR stats for each nonlinear registration tool
# method

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

reg_id_list=(fnirt mmorf ants drtamas mmorf_fa_mask)

for reg_id in ${reg_id_list[@]} 
do
  submit_file="../submit/job_calculate_cvar_stats_${reg_id}.sh"
  if [[ -f ${submit_file} ]];
  then
    rm ${submit_file}
  fi
  touch ${submit_file}
  chmod u+x $submit_file
  # Populate submit script
  echo "./calculate_cvar_stats.sh ${reg_id}" >> ${submit_file}
  # Submit task array job
  fsl_sub -q short -t ${submit_file} -l logs/log_calculate_cvar_stats/${reg_id}
done