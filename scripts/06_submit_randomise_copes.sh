#!/bin/bash
# Run randomise to test group mean activation

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
reg_id_list=(af fnirt mmorf ants drtamas mmorf_fa_mask)

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  echo "Creating ${task_id} jobs"
  for reg_id in ${reg_id_list[@]} 
  do
    for smooth_id in 0
    do
      submit_file="../submit/job_randomise_copes_${task_id}_s${smooth_id}_${reg_id}.sh"
      if [[ -f ${submit_file} ]];
      then
        rm ${submit_file}
      fi
      touch ${submit_file}
      chmod u+x $submit_file
      for cope_id in $(seq ${cope_list[i]})
      do
        # Populate submit script
        echo "./randomise_copes.sh ${task_id} ${cope_id} ${smooth_id} ${reg_id}" >> ${submit_file}
      done
      # Submit task array job
      fsl_sub -q short.qc -t ${submit_file} -l logs/log_randomise_copes/${task_id}_s${smooth_id}_${reg_id}
    done
  done
done