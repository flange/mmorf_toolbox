
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
# Nonlinearly register all subjects using FNIRT
# Pass subject ID as input parameter
source ~/.bash_profile

subject_id=$1
hcp_dir="/well/win-hcp/HCP-YA/subjectsAll"
subject_t1_dir="${hcp_dir}/${subject_id}/T1w"
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
out_dir="${project_dir}/data/${subject_id}/fnirt"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

# FNIRT subject to template
echo ">>> Running FNIRT between subject and template T1w images"
refname="${project_dir}/template/t1"
infname="${subject_t1_dir}/T1w_acpc_dc_restore_brain"
affname="${project_dir}/data/${subject_id}/t1_to_template_affine.mat"
conflevel1="${project_dir}/config/fnirt/T1_2_T1_level1.cnf"
conflevel2="${project_dir}/config/fnirt/T1_2_T1_level2.cnf"
conflevel3="${project_dir}/config/fnirt/T1_2_T1_level3.cnf"
conflevel4="${project_dir}/config/fnirt/T1_2_T1_level4.cnf"
coutlevel1="${out_dir}/warp_coef_level_1"
coutlevel2="${out_dir}/warp_coef_level_2"
coutlevel3="${out_dir}/warp_coef_level_3"
coutlevel4="${out_dir}/warp_coef_level_4"
foutlevel4="${out_dir}/../warp_fnirt"
intname="${out_dir}/intensities"
jout="${out_dir}/../jac_fnirt"

fnirt \
  --ref=$refname \
  --in=$infname \
  --aff=${affname} \
  --config=${conflevel1} \
  --cout=${coutlevel1} \
  --intout=${intname} \
  -v

fnirt \
  --ref=$refname \
  --in=$infname \
  --inwarp=${coutlevel1} \
  --intin=${intname} \
  --config=${conflevel2} \
  --cout=${coutlevel2} \
  -v

fnirt \
  --ref=$refname \
  --in=$infname \
  --inwarp=${coutlevel2} \
  --intin=${intname} \
  --config=${conflevel3} \
  --cout=${coutlevel3} \
  -v

fnirt \
  --ref=$refname \
  --in=$infname \
  --inwarp=${coutlevel3} \
  --intin=${intname} \
  --config=${conflevel4} \
  --cout=${coutlevel4} \
  --fout=${foutlevel4} \
  --jout=${jout} \
  -v

echo ">>> Subject ${subject_id} completed"