#!/usr/bin/env python3
#
# Date: 22/08/2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

import numpy as np
import pandas
from sklearn.metrics import jaccard_score
from skimage.metrics import hausdorff_distance
from fsl.data import image
import argparse

flags = {
    'seg_a' : ('-a', '--seg_a'),
    'seg_b' : ('-b', '--seg_b'),
    'sub_a' : ('-A', '--sub_a'),
    'sub_b' : ('-B', '--sub_b'),
    'lut'   : ('-l', '--lut'),
    'reg'   : ('-r', '--reg'),
    'out'   : ('-o', '--out')
}
helps = {
    'seg_a' : 'Multi-label 3D segmentation image',
    'seg_b' : 'Multi-label 3D segmentation image',
    'sub_a' : 'Name of subject A',
    'sub_b' : 'Name of subject B',
    'lut'   : 'Lookup table of all expected labels in CSV format. 2 columns required with headers "key" and "label"',
    'reg'   : 'Registration method used to align segmentations',
    'out'   : 'Name of output CSV file'
}

parser = argparse.ArgumentParser(description='Calculate Jaccard index and Hausdorff distance between 2 multi-label images')
parser.add_argument(*flags['seg_a'],
                    help=helps['seg_a'],
                    required=True)
parser.add_argument(*flags['seg_b'],
                    help=helps['seg_b'],
                    required=True)
parser.add_argument(*flags['sub_a'],
                    help=helps['sub_a'],
                    required=True)
parser.add_argument(*flags['sub_b'],
                    help=helps['sub_b'],
                    required=True)
parser.add_argument(*flags['lut'],
                    help=helps['lut'],
                    required=True)
parser.add_argument(*flags['reg'],
                    help=helps['reg'],
                    required=True)
parser.add_argument(*flags['out'],
                    help=helps['out'],
                    required=True)
args = parser.parse_args()

# Read in images
im_a = image.Image(args.seg_a)
im_b = image.Image(args.seg_b)

# Read in lookup table and keep only keys found in the images
seg_df = pandas.read_csv(args.lut)
unique_keys = np.unique(np.stack((im_a[:], im_b[:]),axis=-1))
seg_df = seg_df[seg_df['key'].isin(unique_keys)]

# Initialise dataframe fields
seg_df['sub_a'] = args.sub_a
seg_df['sub_b'] = args.sub_b
seg_df['reg'] = args.reg
seg_df['hausdorff'] = np.nan
seg_df['jaccard'] = np.nan

# Calculate Hausdorff distance
for i in seg_df['key']:
    seg_df.loc[(seg_df.key == i), 'hausdorff'] = hausdorff_distance(im_a[:] == i, im_b[:] == i)

# Calculate Jaccard score
tmp_jaccard = jaccard_score(im_a[:].ravel(), im_b[:].ravel(), labels=seg_df['key'].tolist(), average=None)
for (i, j) in zip(seg_df['key'], tmp_jaccard):
    seg_df.loc[(seg_df.key == i), 'jaccard'] = j

# Save output to file
seg_df.to_csv(args.out, sep=',', index=False)    