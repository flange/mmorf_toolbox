
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile


# Calculate cluster mass - i.e. the summed t-statistics within significantly activated
# regions, masked by the grey matter. Now include voxelwise average Jacobian determinant
task_id=$1
max_cope_id=$2
smooth_id=$3
reg_id=$4

project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
out_dir="${project_dir}/data/group/tfmri/${task_id}_hp200_s${smooth_id}/${reg_id}"
mask_file="${project_dir}/template/mask_gm"
jac_file="${project_dir}/data/group/jacs/jac_${reg_id}_mean"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

out_file="${out_dir}/cluster_mass_jac_weighted.txt"
echo "cope mean_tstat n_vox volume mean_90th_tstat" > ${out_file}
for cope_id in $(seq ${max_cope_id})
do
  echo ">>> Running operations for cope ${cope_id}" 
  pval_file="${project_dir}/data/group/tfmri/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_id}/mean_activation_vox_corrp_tstat1"
  tstat_file="${project_dir}/data/group/tfmri/${task_id}_hp200_s${smooth_id}/randomise_cope${cope_id}_${reg_id}/mean_activation_tstat1"
  tstat_masked_file="${out_dir}/cope${cope_id}_tstat1_masked_jac_weighted"

  if [[ ${reg_id} == "af" ]]
  then
    fslmaths \
        ${pval_file} \
        -mul ${mask_file} \
        -thr 0.95 \
        -bin \
        -mul ${tstat_file} \
        ${tstat_masked_file}
  else
    fslmaths \
        ${pval_file} \
        -mul ${mask_file} \
        -thr 0.95 \
        -bin \
        -mul ${tstat_file} \
        -mul ${jac_file} \
        ${tstat_masked_file}
  fi
  
  mean_tstat_masked=$(fslstats ${tstat_masked_file} -M)
  volume_tstat_masked=$(fslstats ${tstat_masked_file} -V)
  mean_90th_tstat_masked=$(fslstats ${tstat_masked_file} -l $(fslstats ${tstat_masked_file} -P 90) -M)
  echo "${cope_id} ${mean_tstat_masked} ${volume_tstat_masked} ${mean_90th_tstat_masked}" >> ${out_file}
done
echo ">>> Completed"