#!/usr/bin/env python3
#
# Date: November 2022
# Author: Frederik J Lange
# Copyright: FMRIB 2022

import argparse
import numpy as np
import nibabel as nib
import pandas as pd
from scipy import stats
from fsl.data.image import Image
from copy import deepcopy

flags = {
    'seg_r' : ('-s_r', '--seg_r'),
    'seg_l' : ('-s_l', '--seg_l'),
    'temp'  : ('-t', '--temp'),
    'lut'   : ('-l', '--lut'),
    'reg'   : ('-r', '--reg'),
    'out'   : ('-o', '--out')
}
helps = {
    'seg_r' : 'Multi-label 3D segmentation image of Right hemisphere',
    'seg_l' : 'Multi-label 3D segmentation image of Left hemisphere',
    'temp'  : 'Template image to which the segmentations were registered',
    'lut'   : 'Lookup table of all expected labels in CSV format. 2 columns required with headers "key" and "label"',
    'reg'   : 'Registration method used to align segmentations',
    'out'   : 'Name of output directory'
}

parser = argparse.ArgumentParser(description='Calculate Peak Area Probability and Captured Area Fractions')
parser.add_argument(*flags['seg_r'],
                    help=helps['seg_r'],
                    required=True)
parser.add_argument(*flags['seg_l'],
                    help=helps['seg_l'],
                    required=True)
parser.add_argument(*flags['temp'],
                    help=helps['temp'],
                    required=True)
parser.add_argument(*flags['lut'],
                    help=helps['lut'],
                    required=True)
parser.add_argument(*flags['reg'],
                    help=helps['reg'],
                    required=True)
parser.add_argument(*flags['out'],
                    help=helps['out'],
                    required=True)
args = parser.parse_args()

# Read in images
print('Reading in images')
im_r = Image(args.seg_r)
im_l = Image(args.seg_l)
im_t = Image(args.temp)

# Merge data from right and left segmentations
print('Merging hemispheres')
data_rl = np.concatenate((im_r[:], im_l[:]), axis=3, dtype=np.int16)

# Calculate probabilities of all labels including "0" (no label)
print('Calculating probability maps')
data_prob = np.zeros(im_t.shape + (361,), dtype=np.float32)
for i in range(361):
    data_prob[..., i] = np.sum(data_rl == i, axis=3, dtype=np.float32)/100.0

# Handle the fact that at least 50% of voxels will have "0" label since we've merged right and left
data_prob[..., 0] = data_prob[..., 0] - 1.0

# Save out probability maps as a 4D volume
print('Saving probability maps')
nib_im_prob = nib.Nifti1Image(data_prob, im_t.nibImage.affine, im_t.nibImage.header)
nib_im_prob.to_filename(args.out + '/mmp1_probablity_map_' + args.reg + '.nii.gz')

# Find index (corresponding to label) with highest probability, excluding "0" (no label)
print('Calculating hard segmentation')
data_prob_argmax = np.argmax(data_prob[..., 1:361], axis=3)

# Convert argmax indices to labels for areas where p(0) <= 0.5 (i.e., grey matter voxels)
data_prob_hard_seg = np.zeros(im_t.shape, dtype=np.int16)
data_prob_hard_seg[data_prob[..., 0] <= 0.5] = data_prob_argmax[data_prob[..., 0] <= 0.5] + 1

# Save out hard segmentation as a 3D volume
print('Saving hard segmentation')
nib_im_prob_hard_seg = nib.Nifti1Image(data_prob_hard_seg, im_t.nibImage.affine, im_t.nibImage.header)
nib_im_prob_hard_seg.to_filename(args.out + '/mmp1_hard_segmentation_' + args.reg + '.nii.gz')

# Calculate uncertainty map (including "0" no-label)
print('Calculating uncertainty map')
data_prob_max = np.max(data_prob, axis=3)
data_prob_uncertainty = 1 - data_prob_max

# Save out uncertainty map as a 3D volume
print('Saving uncertainty map')
nib_im_prob_uncertainty = nib.Nifti1Image(data_prob_uncertainty, im_t.nibImage.affine, im_t.nibImage.header)
nib_im_prob_uncertainty.to_filename(args.out + '/mmp1_uncertainty_' + args.reg + '.nii.gz')

# Calculate Peak Area Probability
print('Calculating Peak Area Probability')
pap = np.zeros(360, dtype=np.float32)
for i in range(360):
    pap[i] = np.max(data_prob[..., i+1])

# Calculate Captured Area Fraction
print('Calculating Captured Area Fraction')
caf = np.zeros(360, dtype=np.float32)
for i in range(360):
    caf[i] = np.sum(data_prob[..., i+1][data_prob_hard_seg == (i+1)]) / np.sum(data_prob[..., i+1])

# Read in lookup table for segementation labels. Should be under the headings "key" and "label"
print('Reading in lookup table')
seg_df = pd.read_csv(args.lut)

# Initialise dataframe fields
print('Initialising dataframe')
seg_df['reg'] = args.reg
seg_df['pap'] = np.nan
seg_df['caf'] = np.nan

# Populate dataframe fields
print('Populating dataframe')
for i in seg_df['key']:
    seg_df.loc[(seg_df.key == i), 'pap'] = pap[i - 1]
    seg_df.loc[(seg_df.key == i), 'caf'] = caf[i - 1]

# Save output to file
print('Saving out CSV')
seg_df.to_csv(args.out + '/mmp1_metrics_mmp_' + args.reg + '.csv', sep=',', index=False)