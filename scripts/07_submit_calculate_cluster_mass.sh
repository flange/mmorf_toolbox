#!/bin/bash
# Calculate cluster weights - i.e. the summed t-statistics within significantly activated
# regions, weighted by the grey matter partial volume estimates

# Create submit directory and file
if ! [[ -d ../submit ]];
then
  mkdir -p ../submit
fi

task_list=(tfMRI_EMOTION tfMRI_GAMBLING tfMRI_LANGUAGE tfMRI_MOTOR tfMRI_RELATIONAL tfMRI_SOCIAL tfMRI_WM)
cope_list=(6 6 6 26 6 6 30)
reg_id_list=(af fnirt mmorf ants drtamas mmorf_fa_mask)

for i in ${!task_list[@]}
do
  task_id=${task_list[i]}
  max_cope_id=${cope_list[i]}
  for smooth_id in 0
  do
    submit_file="../submit/job_calculate_cluster_mass_${task_id}_s${smooth_id}.sh"
    if [[ -f ${submit_file} ]];
    then
      rm ${submit_file}
    fi
    touch ${submit_file}
    chmod u+x $submit_file
    for reg_id in ${reg_id_list[@]}
    do
      echo "./calculate_cluster_mass.sh ${task_id} ${max_cope_id} ${smooth_id} ${reg_id} " >> ${submit_file}
    done
    fsl_sub -q short.qc -t ${submit_file} -l logs/log_calculate_cluster_mass/${task_id}_s${smooth_id}
  done
done