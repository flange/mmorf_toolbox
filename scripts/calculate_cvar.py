#!/usr/bin/env python3
#
# Date: 15/08/2023
# Author: Frederik J Lange
# Copyright: FMRIB 2023

import copy
import numpy as np
from fsl.data.image import Image
import argparse

flags = {
    'warp'  : ('-w', '--warp'),
    'ref'   : ('-r', '--ref'),
    'out'   : ('-o', '--out'),
    'itk'   : ('-i', '--itk')
}
helps = {
    'warp'  : 'Name of input warp (4D) file',
    'ref'   : 'Name of input reference (3D) image',
    'out'   : 'Name of output CVAR file',
    'itk'   : 'Warp is in ITK format'
}

parser = argparse.ArgumentParser(description='Calculate the Cube-Volume Aspect Ratio (CVAR) from a given warp field')
parser.add_argument(*flags['warp'],
                    help=helps['warp'],
                    required=True)
parser.add_argument(*flags['ref'],
                    help=helps['ref'],
                    required=True)
parser.add_argument(*flags['out'],
                    help=helps['out'],
                    required=True)
parser.add_argument(*flags['itk'],
                    help=helps['out'],
                    action='store_true',
                    required=False)
args = parser.parse_args()

# Read in images
im_warp = Image(args.warp)
im_ref = Image(args.ref)

# Read in warp data
data_warp = np.squeeze(im_warp[:])
if args.itk:
    data_warp[:,:,:,1] = -data_warp[:,:,:,1]

# Calculate Jacobian matrix
jac = np.moveaxis(np.array(np.gradient(data_warp,axis=(0,1,2),edge_order=2)),0,-1)
jac[:,:,:,0,0] += 1
jac[:,:,:,1,1] += 1
jac[:,:,:,2,2] += 1

# Calculate singular values
s_vals = np.linalg.svd(jac, compute_uv=False)

# Calculate CVAR
cvar = np.cbrt((s_vals[:,:,:,0]**3)/np.prod(s_vals, axis=3))

# Save out CVAR
im_cvar_out = copy.deepcopy(im_ref)
im_cvar_out[:] = cvar
im_cvar_out.save(args.out)