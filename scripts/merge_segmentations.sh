
#!/bin/bash
echo "------------------------------------------------"
echo "SGE Job ID: ${JOB_ID}"
echo "SGE Task ID: ${SGE_TASK_ID}"
echo "Run on host: "$(hostname)
echo "Operating system: "$(uname -s)
echo "Username: "$(whoami)
echo "Started at: "$(date)
echo "------------------------------------------------"
echo ""
source ~/.bash_profile

# Merge warped FS and MMP MMP1.0 parcellations
reg_id=$1
project_dir="/well/win-fmrib-analysis/users/sgk882/mmorf_toolbox"
in_files_fs=$(ls ${project_dir}/data/*/seg/fs/aparc.a2009s+aseg_${reg_id}.nii.gz)
in_files_mmp_left=$(ls ${project_dir}/data/*/seg/mmp/hcp_mmp_seg_L_${reg_id}.nii.gz)
in_files_mmp_right=$(ls ${project_dir}/data/*/seg/mmp/hcp_mmp_seg_R_${reg_id}.nii.gz)
out_dir="${project_dir}/data/group/seg/${reg_id}"
out_file_fs="${out_dir}/aparc.a2009s+aseg_${reg_id}_merged"
out_file_mmp_left="${out_dir}/hcp_mmp_seg_L_${reg_id}_merged"
out_file_mmp_right="${out_dir}/hcp_mmp_seg_R_${reg_id}_merged"

# Create output directory
echo ">>> Creating directory ${out_dir}"
if ! [[ -d ${out_dir} ]];
then
  mkdir -p ${out_dir}
else
  echo "Directory already exists"
fi

echo ">>> Running fslmerge" 
fslmerge \
  -t \
  ${out_file_fs} \
  ${in_files_fs}

fslmerge \
  -t \
  ${out_file_mmp_left} \
  ${in_files_mmp_left}

fslmerge \
  -t \
  ${out_file_mmp_right} \
  ${in_files_mmp_right}

echo ">>> Completed"